<?php 

include('include/include.php');

$periode=$_SESSION['periode'] ;

echo '<h1>' . $titre_du_site . '</h1>' 
        . '<h3>Du ' . $_SESSION['periode'][0]->format('j/m/Y') .' Au '  . $_SESSION['periode'][1]->format('j/m/Y') . '</h3>' ;  

// on explique l'histoire du journal
echo "Pour que cette fonctionnalité fonctionne vous devez avoir ajouter un journal dont le code est CL (vous pouvez le désactivé pour éviter des écritures inoportune, ce journal est utilisé uniquement pour les écriture de cloture (RAZ des comptes). <br><br>" ;

//on vérifie que le journal cloture existe sinon on quitte
if(!verif_journal_cloture($bdd)) 
    {exit("Votre version de dolibarr ne contient pas de journal cloture merci de le créer<br>");}

//on verifie qu'il y a des ecriture dans le grand livre
if(!verif_ecriture($bdd)) 
    {exit("Votre version de dolibarr ne contient pas d'écriture dans le grand livre pour le moment<br>");}

//Nombre d'ecriture
$nombreecriture=verif_ecriture_annee($bdd,$periode) ;
$nombreecriturecloture=verif_ecriture_cloture_annee($bdd,$periode) ;
$controle=controle_cloture_annee($bdd,$periode) ;

echo '<h3>Sur la période du ' . $_SESSION['periode'][0]->format('j/m/Y') .' Au ' . $_SESSION['periode'][1]->format('j/m/Y') . '</h3>' ;
if ($nombreecriture[0])  
    {
    echo 'Il y a ' . $nombreecriture[0] . ' dans le grand livre<br>';
    if ($nombreecriturecloture[0]) 
        {
        echo 'Il y a ' . $nombreecriturecloture[0] . ' dans le journal de cloture<br>' ;
        if(empty($controle)) echo 'Les comptes sont soldés sur la période' ;
        else 
            {
            echo 'Les comptes ne sont pas soldés des écritures ont été ajouté aprés la clotûre dans les comptes suivants : ' ;
            foreach($controle as $compte)
                echo $compte . ', ' ;
            }
        }
    else echo 'Il n\'y a pas d\'écriture dans le journal cloture, la cloture n\'est pas faites' ;    
    }
else echo 'Il n\'y a pas d\'écriture dans le grand livre pas de cloture à faire';

echo "</body></html>" ;



function verif_journal_cloture($bdd)  //on vérifie que le journal cloture existe sinon on quitte retourne 1 si oui 0 si non

{
$ecriture = $bdd->prepare('SELECT `code` FROM `llx_accounting_journal` WHERE code="CL"');
$ecriture->execute();
if(!$donnees = $ecriture->fetch())     $cloture=0;
else {$cloture=1;}
$ecriture->closeCursor(); 
return $cloture;
}

function verif_ecriture($bdd)  // on verifie qu'il y a des écriture dans le grand livre
{
$ecriture = $bdd->prepare('SELECT * FROM `llx_accounting_bookkeeping`');
$ecriture->execute();
if(!$donnees = $ecriture->fetch()) {$ecriture_presente=0;}
else {$ecriture_presente=1;}
$ecriture->closeCursor(); 
return $ecriture_presente;
}


function verif_ecriture_annee($bdd,$periode) // on vérifie qu'il y a des ecriture dans le grand livre la période demandé on retourne le nombre
{
$ecriture = $bdd->prepare('SELECT COUNT(*) FROM `llx_accounting_bookkeeping` WHERE `doc_date` BETWEEN ? AND ?');
$ecriture->execute(array($periode[0]->format('Y-m-d') , $periode[1]->format('Y-m-d') ));
$donnees = $ecriture->fetch() ;
$ecriture->closeCursor();
return $donnees;
}


function verif_ecriture_cloture_annee($bdd,$periode) // on vérifie qu'il y a des ecriture dans le journal cloture sur la période demandé on retourne le nombre
{
$ecriture = $bdd->prepare('SELECT COUNT(*) FROM `llx_accounting_bookkeeping` WHERE `doc_date` BETWEEN ? AND ? AND code_journal = ?');
$ecriture->execute(array($periode[0]->format('Y-m-d') , $periode[1]->format('Y-m-d') ,"CL"));
$donnees = $ecriture->fetch() ;
$ecriture->closeCursor();
return $donnees;
}


function controle_cloture_annee($bdd,$periode)  //controle que la cloture est bien faite cad que le solde de chaque compte est bien nul
//return 1 si la cloture est correcte sinon un array avec les numeros de compte non équilibré
{
$compteerreur=array();
$listecompte=lister_compte($bdd,$periode) ;
foreach($listecompte as $compte)
    {
    $parametre=parametre_compte($bdd,$periode,$compte,1) ;
    if(!($parametre['totaldebit']==$parametre['totalcredit']))
        $compteerreur[]=$compte ;
    }
    return $compteerreur ; 
}


?>
