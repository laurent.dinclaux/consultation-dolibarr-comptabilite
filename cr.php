<?php 
include('include/include.php') ;
//variable pour les totaux
$total_debit=0;
$total_credit=0;

//liste des comptes de charges et produits
$listecomptecharge=lister_compte($bdd,$_SESSION['periode'],$preg="^[6]") ;
$listecompteproduit=lister_compte($bdd,$_SESSION['periode'],$preg="^[7]") ;

//tableau des charges
echo '<h1>' . $titre_du_site . '</h1>' 
        . '<h3>Du ' . $_SESSION['periode'][0]->format('j/m/Y') .' Au '  .  $_SESSION['periode'][1]->format('j/m/Y') . '</h3>' ;  

echo "<h2>Compte de resultat</h2><div id=\"global\"><div id=\"gauche\"><table border=4 cellpading=50 align=center><h3>Charges</h3>" ;

foreach($listecomptecharge as $compte) // pour chaque compte de charges
    {
    $parametre=parametre_compte($bdd,$_SESSION['periode'],$compte,0) ; // on recupere les parametre du compte
    if ($parametre['soldecredit'])
        echo '<tr align=center ><td>' . $compte . '</td><td>' . $parametre['label'] . '</td><td>' . $parametre['soldecredit'] . ' €</td></tr>'; // on les affiche
    $total_debit += $parametre['soldecredit'] ; // on met a jour le total
    }
// la ligne du total
echo "</table><h3>Total charges: " . $total_debit . " €</h3></div>" ; 

//tableau des produits
echo "<div id=\"droite\"><h3>Produits</h3><table border=4 cellpading=50 align=center>";

foreach($listecompteproduit as $compte)
    {
    $parametre=parametre_compte($bdd,$_SESSION['periode'],$compte,0) ; // on recupere les parametre du compte
    if ($parametre['soldedebit'])
        echo '<tr align=center ><td>' . $compte . '</td><td>' . $parametre['label']. '</td><td>' . $parametre['soldedebit'] ." €</td></tr>" ; //on les affciche
    $total_credit  += $parametre['soldedebit'] ; // on met a jour le total
    }

echo "</table><h3>Total produits: " . $total_credit . " €</h3></div></div>" ; // la ligne du total
$resultat =  round($total_credit-$total_debit,2) ; //calcul du resultat
echo '<div id="pied"><h2>Résultat :  ' . $resultat . "  €</h2></div></body></html>";
?>
